This README aims to explain how to use this thesis template
=========================================


Requirements
------
1- TexLive installed



General Information about this thesis template
-----

Look at the example prebuilt as mainbody.pdf

All the packages used and overall document layout are defined in mainbody.tex

To build the document, you need to call the make command in the terminal. 
The command must be called 3 or 4 times consecutively for the document to built the references and tables of content correctly.

The content for each element of the thesis (abstracts, intro, study...) are contained in separate files e.g. abstract.tex, introduction.tex, study1.tex etc.


Acronyms
-----
Acronyms to be used for abbreviations with the \gls and \glspl commands are located in the acronyms.tex file.
Add/Amend existing acronyms by adding or modifying a corresponding \newacronym{EG}{EG}{Example Acronym} line in the file

References
-----
References to be cited with \citet{} or \citep{} are located in the library.bib file.
The format is the default BibTex format that can be exported from all reference managers (e.g. Mendeley or Zotero)

Notes
----
In this template, appendices were designed to be chapter specific. One might want to make them general and add them all in a separate chapter at the end.